package CousreWork.Malik;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class MainClass extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = (AnchorPane) FXMLLoader.load(getClass().getClassLoader().getResource(("CousreWork/Malik/View/AppOverview.fxml")));
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);


            primaryStage.setTitle("Построение минимального остовного дерева");
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}