package CousreWork.Malik.SupplementaryClass;

import java.awt.Point;
import java.util.*;

public class KruskalAlgorithm {
    public int numberOfVertices, numberOfEdges;
    public ArrayList<Edge> edge;

    public KruskalAlgorithm() {

    }

    public KruskalAlgorithm(int v, int e) {
        this.numberOfVertices = v;
        this.numberOfEdges = e;
        edge = new ArrayList<>();

        for (int i = 0; i < e; ++i)
            edge.add(new Edge());
    }

    public PointKruskal find(Subset Subsets[], PointKruskal i) {
        if (Subsets[i.number].parent != i)
            Subsets[i.number].parent = find(Subsets, Subsets[i.number].parent);
        return Subsets[i.number].parent;
    }

    private void union(Subset Subsets[], PointKruskal x, PointKruskal y) {
        PointKruskal xroot = find(Subsets, x);
        PointKruskal yroot = find(Subsets, y);

        if (Subsets[xroot.number].rank.number < Subsets[yroot.number].rank.number)
            Subsets[xroot.number].parent = yroot;
        else if (Subsets[xroot.number].rank.number > Subsets[yroot.number].rank.number)
            Subsets[yroot.number].parent = xroot;
        else {
            Subsets[yroot.number].parent = xroot;
            Subsets[xroot.number].rank.number++;
        }
    }

    public ArrayList<Edge> KruskalMST() {
        ArrayList<Edge> result = new ArrayList<>();
        int e = 0;
        int i;

        for (i = 0; i < numberOfVertices; ++i)
            result.add(new Edge());

        Collections.sort(result);
        Subset Subsets[] = new Subset[numberOfVertices];

        for(i = 0; i < numberOfVertices; ++i)
            Subsets[i] = new Subset();

        for (int v = 0; v < numberOfVertices; ++v) {
            Subsets[v].parent.number = v;
            Subsets[v].rank.number = 0;
        }

        i = 0;
        while (e < (numberOfVertices - 1))
        {
            Edge next_edge;
            next_edge = edge.get(i++);

            PointKruskal x = find(Subsets, next_edge.start);
            PointKruskal y = find(Subsets, next_edge.end);
            if (x != y) {
                result.set(e++, next_edge);
                union(Subsets, x, y);
            }
        }
        // чтобы дебажить фалгоритм
        //System.out.println("Following are the edges in the constructed MST");
        //for (i = 0; i < result.size() - 1; ++i)
          //  System.out.println(result.get(i).start.number +" -- "+result.get(i).end.number +" == "+
           //         result.get(i).weight);
        return result;

    }

    public class PointKruskal extends Point {
        public int number;

        public PointKruskal(int sX, int sY) {
            new Point(sX, sY);
        }

        public PointKruskal() {

        }
    }

    public class Edge implements Comparable<Edge> {
        public PointKruskal start, end;
        public int weight;

        public Edge() {
            start = new PointKruskal();
            end = new PointKruskal();
        }

        public Edge(int sX, int sY, int eX, int eY){
            start = new PointKruskal(sX, sY);
            end = new PointKruskal(eX, eY);
        }

        public int compareTo(Edge compareEdge) {
            return this.weight - compareEdge.weight;
        }
    }

    public class Subset {
        public PointKruskal parent, rank;

        public Subset(){
            parent = new PointKruskal();
            rank = new PointKruskal();
        }
    }
}
