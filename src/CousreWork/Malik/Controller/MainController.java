package CousreWork.Malik.Controller;


import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import CousreWork.Malik.SupplementaryClass.KruskalAlgorithm;

public class MainController {

    @FXML
    Pane mainPane;

    private List<Circle> listOfCircles = new ArrayList<>();
    ArrayList<Line> listOfEdges = new ArrayList<>();
    ArrayList<Integer> startOfEdge;
    ArrayList<Integer> endOfEdge;
    ArrayList<KruskalAlgorithm.Edge> readyEdgeList;
    ArrayList<Line> tmpEnd;
    ArrayList<Line> tmpStart;

    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;

    int amountOfVertices = 256;
    int amountOfEdges;
    int stepCounter = 0;
    int circleIndex = -1;

    @FXML
    public void readFromVideoFile() throws IOException {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);

        if (file != null) {
            byte[] arr = readByte(file);
            int[] intByte = new  int[arr.length];

            int counter = 0;
            for(int i = 0; i < arr.length; i++) {
                if (arr[i] < 0) {
                    intByte[counter] = (arr[i] + 256);
                }
                else {
                    intByte[counter] = ((int) arr[i]);
                }
                counter++;
            }
            for(int x : intByte) {//нужно удалить позже
                System.out.print(x + " ");
            }
            startOfEdge = new ArrayList<>();
            endOfEdge = new ArrayList<>();

            for (int i = 0; i < intByte.length; i+=2) {
                    startOfEdge.add(intByte[i]);
                    endOfEdge.add(intByte[i+1]);
            }
            amountOfEdges = ((startOfEdge.size()-1) + (endOfEdge.size()-1))/2;
            drawVertices();
        }
    }

    public byte[] readByte(File file) throws IOException {
        byte[] buffer = {};

        try(FileInputStream fis = new FileInputStream(file)){
                buffer = new byte[fis.available()];
                fis.read(buffer, 0, fis.available());
            return buffer;
        }
        catch (FileNotFoundException fnfex){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("File not found");
            alert.showAndWait();
        }
        catch (IOException ioex){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Some problems with reading!");
            alert.showAndWait();
        }
        finally {
            return buffer;
        }
    }

    public void drawVertices() {
        double xTranslate = mainPane.getWidth() / 2;
        double yTranslate = mainPane.getHeight() / 2;
        double phi0 = 0;
        double phi = 2 * Math.PI / amountOfVertices;
        int r = 200;

        for (int i = 0; i < amountOfVertices; i++) {
            Circle c = new Circle(( (int)(((100 + r * Math.cos(phi0))*1.4) + (xTranslate*0.55)) ), (int)(( (100 + r * (Math.sin(phi0)))*1.4) + (yTranslate*0.525)), 3.5);

            c.setFill(Color.DARKGRAY);
            c.setStrokeWidth(0);
            c.setOnMousePressed(circleOnMousePressedEventHandler);
            c.setOnMouseDragged(circleOnMouseDraggedEventHandler);
            c.setOnMouseReleased(circleOnMouseDraggedReleaseEventHandler);
            c.setOnMouseReleased(circleOnMouseReleased);
            c.setOnMouseClicked(circleOnMouseDoubleClicked);

            c.setCursor(Cursor.HAND);
            listOfCircles.add(c);
            phi0 += phi;
        }
        mainPane.getChildren().addAll(listOfCircles);
    }

    @FXML
    public void drawMST() {
        try {
            KruskalAlgorithm graph = new KruskalAlgorithm(amountOfVertices, amountOfEdges);
            initializeGraph(graph);
            readyEdgeList = graph.KruskalMST();

            for (int i = stepCounter; i < readyEdgeList.size(); i++) {
                System.out.println();
                System.out.println(readyEdgeList.get(i).start.x + " " + readyEdgeList.get(i).start.y + " "
                                    + readyEdgeList.get(i).end.x + " " + readyEdgeList.get(i).end.y);

                Line line = new Line(readyEdgeList.get(i).start.x, readyEdgeList.get(i).start.y, readyEdgeList.get(i).end.x, readyEdgeList.get(i).end.y);
                line.setStrokeWidth(1);
                listOfEdges.add(line);
                mainPane.getChildren().add(line);
                stepCounter++;
            }

        }
        catch (Exception e){
            e.printStackTrace();
            showNoDataError();
        }
    }


    public void initializeGraph(KruskalAlgorithm graph){

        for (int i = 0; i < amountOfEdges; i++) {

            graph.edge.get(i).start.number = startOfEdge.get(i);
            graph.edge.get(i).end.number = endOfEdge.get(i);

            graph.edge.get(i).start.x = (int) listOfCircles.get(graph.edge.get(i).start.number).getCenterX();
            graph.edge.get(i).start.y = (int) listOfCircles.get(graph.edge.get(i).start.number).getCenterY();

            graph.edge.get(i).end.x = (int) listOfCircles.get(graph.edge.get(i).end.number).getCenterX();
            graph.edge.get(i).end.y = (int) listOfCircles.get(graph.edge.get(i).end.number).getCenterY();

            graph.edge.get(i).weight = 1;

        }
    }

    @FXML
    public void deleteIteration() {

        mainPane.getChildren().remove(listOfEdges.get(stepCounter));
        stepCounter--;
    }

    @FXML
    public void drawNextIteration() {
        try {
            KruskalAlgorithm graph = new KruskalAlgorithm(amountOfVertices, amountOfEdges);
            initializeGraph(graph);
            readyEdgeList = graph.KruskalMST();
            if (stepCounter == readyEdgeList.size()){
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Внимание!");
                alert.setHeaderText(null);
                alert.setContentText(
                        "Дерево построено.");
                alert.showAndWait();
                return;
            }
                Line line = new Line(readyEdgeList.get(stepCounter).start.x, readyEdgeList.get(stepCounter).start.y, readyEdgeList.get(stepCounter).end.x, readyEdgeList.get(stepCounter).end.y);
                line.setStrokeWidth(1);
                listOfEdges.add(line);
                mainPane.getChildren().add(line);
            stepCounter++;
            }
        catch (Exception e){
            e.printStackTrace();
            showNoDataError();
        }
    }

    @FXML
    public void saveToFile() {
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showSaveDialog(null);
        String pointSetAsString = "";
        for (int i = 0; i < readyEdgeList.size(); i++) {
            pointSetAsString += readyEdgeList.get(i).start.x + " " + readyEdgeList.get(i).start.y + " "
                    + readyEdgeList.get(i).end.x + " " + readyEdgeList.get(i).end.y;
            pointSetAsString += "\n";
        }

        try(FileWriter fw = new FileWriter(file)) {

            if (file != null) {
                fw.write(pointSetAsString);
            }
        }
        catch (IOException ex){
            ex.printStackTrace();
        }

    }

    @FXML
    public void openFile() {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);
        mainPane.getChildren().clear();
        listOfEdges = new ArrayList<Line>();
        readyEdgeList = new ArrayList<>();
        listOfCircles = new ArrayList<>();
        drawVertices();

        try (BufferedReader br = new BufferedReader(new FileReader(file))){
            String s = "";
            while((s = br.readLine()) != null) {
                String[] sArr = s.split(" ");
                KruskalAlgorithm.Edge ed = new KruskalAlgorithm().new Edge();
                ed.start.x = Integer.parseInt(sArr[0]);
                ed.start.y = Integer.parseInt(sArr[1]);
                ed.end.x = Integer.parseInt(sArr[2]);
                ed.end.y = Integer.parseInt(sArr[3]);
                readyEdgeList.add(ed);
            }
            for (int i = 0; i < readyEdgeList.size(); i++) {
                Line line = new Line(readyEdgeList.get(i).start.x, readyEdgeList.get(i).start.y, readyEdgeList.get(i).end.x, readyEdgeList.get(i).end.y);
                line.setStrokeWidth(1);
                listOfEdges.add(line);
                mainPane.getChildren().add(line);
            }
        }
        catch (IOException e){

        }
    }

    @FXML
    public void showAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("О программе");
        alert.setHeaderText(null);
        alert.setContentText(
                "Программа дял построения минимального остовного дерева алгоритмом Крускала.\nАвтор - Иван Малик.\nПрава не защищены. Программа в свободном доступе.");
        alert.showAndWait();
    }

    @FXML
    public void showHTU() {
        try {
            File pdfFile = new File("C://HTU.pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                } else {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Awt Desktop is not supported!");
                    alert.showAndWait();
                }

            } else {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Some problems with file!");
                alert.showAndWait();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    //В разработке
    public void createReport() {
            if(readyEdgeList.size() == 0){
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Постройте алгоритм");
                alert.showAndWait();
                return;
            }
        String s = "Отчет.html";
        try {
            FileWriter sw = new FileWriter(s);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    public void cleanPain() {
        mainPane.getChildren().clear();
        listOfCircles.clear();
        listOfEdges.clear();
        startOfEdge.clear();
        endOfEdge.clear();
        stepCounter = 0;
    }

    @FXML
    public void showNoDataError() {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("No data!");
        alert.setHeaderText(null);
        alert.setContentText("Error");
        alert.showAndWait();
    }

    EventHandler<MouseEvent> circleOnMousePressedEventHandler = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent t) {
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();

            orgTranslateX = ((Circle)(t.getSource())).getTranslateX();
            orgTranslateY = ((Circle)(t.getSource())).getTranslateY();
            Circle tmpCircle = (Circle) t.getSource();

            tmpEnd = new ArrayList<>();
            tmpStart = new ArrayList<>();

            for ( int i = 0; i < listOfCircles.size(); i++){
                if ((listOfCircles.get(i).getCenterX() == tmpCircle.getCenterX()) && (listOfCircles.get(i).getCenterY() == tmpCircle.getCenterY())){
                    circleIndex = i;
                    System.out.println("Сраный индекс = " + circleIndex);
                }
            }

            for(int i = 0; i < listOfEdges.size(); i++ ) {
                if ( (listOfEdges.get(i).getEndX() == tmpCircle.getCenterX()) && (listOfEdges.get(i).getEndY() == tmpCircle.getCenterY()) ) {
                    System.out.println("------------------------");
                    System.out.println(listOfEdges.get(i).getEndX() + " " + tmpCircle.getCenterX() + " " + listOfEdges.get(i).getEndY() + " " + tmpCircle.getCenterY());
                    tmpEnd.add(listOfEdges.get(i));
                    System.out.println(tmpEnd);
                }
                else if ((listOfEdges.get(i).getStartX() == tmpCircle.getCenterX()) && (listOfEdges.get(i).getStartY() == tmpCircle.getCenterY()) ){
                    System.out.println("------------------------");
                    System.out.println(listOfEdges.get(i).getStartX() + " " + tmpCircle.getCenterX() + " " + listOfEdges.get(i).getStartY() + " " + tmpCircle.getCenterY());
                    tmpStart.add(listOfEdges.get(i));
                    System.out.println(tmpStart);
                }
            }
            ((Circle) (t.getSource())).setFill(Color.RED);
        }
    };

    EventHandler<MouseEvent> circleOnMouseDraggedEventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent t) {
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;

            ((Circle)(t.getSource())).setTranslateX(newTranslateX);
            ((Circle)(t.getSource())).setTranslateY(newTranslateY);
            ((Circle) (t.getSource())).setFill(Color.RED);

           for (int i = 0; i < tmpStart.size(); i++){
               tmpStart.get(i).setStartX(listOfCircles.get(circleIndex).getTranslateX() + listOfCircles.get(circleIndex).getCenterX());
               tmpStart.get(i).setStartY(listOfCircles.get(circleIndex).getTranslateY() + listOfCircles.get(circleIndex).getCenterY());
           }
            for (int i = 0; i < tmpEnd.size(); i++) {
                tmpEnd.get(i).setEndX(listOfCircles.get(circleIndex).getTranslateX() + listOfCircles.get(circleIndex).getCenterX());
                tmpEnd.get(i).setEndY(listOfCircles.get(circleIndex).getTranslateY() + listOfCircles.get(circleIndex).getCenterY());
            }

        }
    };

    EventHandler<MouseEvent> circleOnMouseDraggedReleaseEventHandler = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent t) {
            ((Circle) (t.getSource())).setFill(Color.BLACK);
        }
    };

    EventHandler<MouseEvent> circleOnMouseReleased = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            ((Circle) (event.getSource())).setFill(Color.DARKGRAY);
        }
    };

    EventHandler<MouseEvent> circleOnMouseDoubleClicked = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            if (event.getClickCount() == 2){
                Circle tmpCircle = (Circle) event.getSource();
                for(int i = 0; i < listOfEdges.size(); i++ ) {
                    if ( (listOfEdges.get(i).getEndX() == tmpCircle.getCenterX()) && (listOfEdges.get(i).getEndY() == tmpCircle.getCenterY()) ) {
                        tmpEnd.add(listOfEdges.get(i));
                    }
                    else if ((listOfEdges.get(i).getStartX() == tmpCircle.getCenterX()) && (listOfEdges.get(i).getStartY() == tmpCircle.getCenterY()) ){
                        tmpStart.add(listOfEdges.get(i));
                    }
                }
                for (int i = 0; i < tmpEnd.size(); i ++){
                    mainPane.getChildren().remove(tmpEnd.get(i));
                }
                for (int i = 0; i < tmpStart.size(); i ++){
                    mainPane.getChildren().remove(tmpStart.get(i));
                }
                mainPane.getChildren().remove(event.getSource());
            }
        }
    };
}
